const express = require('express')
const router = express.Router()
const mysql = require('mysql')

const conn = mysql.createConnection({
    host:'localhost',
    user:'root',
    password:'',
    database:'anthony'
})

conn.connect(function(err){
    if(err)
        throw err
})

router.post('/',function(req,res){
    const sql = `INSERT INTO items (nama) VALUES (\'${req.body.deskripsi}\')`
    conn.query(sql,function(err){
        if(err) 
            throw err
        console.log('Add Data Success')
    })
    res.sendStatus(200)
})

router.get('/', function(req,res){
    const sql = 'SELECT * FROM items'
    conn.query(sql,function(err,result){
        if(err)
            throw err
        res.send(result)
        console.log(result)
    })
})

router.delete('/:deskripsi',function(req,res){
    const query = `DELETE FROM items WHERE nama=\'${req.params.deskripsi}\'`
    conn.query(query,function(err,result){
        if(err)
            throw err
        res.send("Delete Success")
    })
})

module.exports = router